# Ubuntu 14.04 [![build status](https://gitlab.com/nvidia/opencl/badges/ubuntu14.04/build.svg)](https://gitlab.com/nvidia/opencl/commits/ubuntu14.04)

- [`runtime-ubuntu14.04` (*runtime/Dockerfile*)](https://gitlab.com/opencl/cuda/blob/ubuntu14.04/runtime/Dockerfile)
- [`devel-ubuntu14.04` (*devel/Dockerfile*)](https://gitlab.com/nvidia/opencl/blob/ubuntu14.04/devel/Dockerfile)
